defmodule Crypto do
  def md5(data) do
    Base.encode16(:erlang.md5(data), case: :lower)
    :crypto.hash(:md5, data) |> Base.encode16()
  end

  def sha256(data) do
    Base.encode16(:erlang.md5(data), case: :lower)
    :crypto.hash(:sha256, data) |> Base.encode16()
  end
end
