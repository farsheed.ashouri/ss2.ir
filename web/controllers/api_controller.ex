defmodule Ss2.ApiController do
  use Ss2.Web, :controller

  def ping(conn, _params) do
    json(conn, %{:message => :pong})
  end

  def save(conn, params) do
    case params["link"] do
      nil ->
        json(conn, %{:message => :link_not_found})

      link ->
        shortlink = Database.Link.get(link)
        json(conn, %{:shortlink => shortlink})
    end
  end

  def stats(conn, params) do
    keys = params["keys"]

    data =
      for key <- keys do
        case Database.Link.from_code(key) do
          :not_found ->
            %{key: key, clicks: -1}

          [url, clicks] ->
            %{link: url, clicks: clicks, key: "https://ss2.ir/#{key}"}
        end
      end

    conn |> json(%{results: data})
  end
end
